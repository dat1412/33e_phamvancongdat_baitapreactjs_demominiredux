import { GIAM_SO_LUONG, TANG_SO_LUONG } from "../constants/numberConstant";

export const tangSoLuongAction = () => {
  return {
    type: TANG_SO_LUONG,
    payload: 1,
  };
};

export const giamSoLuongAction = () => {
  return {
    type: GIAM_SO_LUONG,
    payload: 1,
  };
};
