import React, { Component } from "react";
import { connect } from "react-redux";
import {
  giamSoLuongAction,
  tangSoLuongAction,
} from "./redux/actions/numberAction";

class Demo_Redux_Mini extends Component {
  render() {
    return (
      <div className="container py-5">
        <button onClick={this.props.giamSoLuong} className="btn btn-danger">
          -
        </button>
        <span className="mx-3">{this.props.number}</span>
        <button onClick={this.props.tangSoLuong} className="btn btn-success">
          +
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    number: state.numberReducer.number,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    tangSoLuong: () => {
      dispatch(tangSoLuongAction());
    },
    giamSoLuong: () => {
      dispatch(giamSoLuongAction());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Demo_Redux_Mini);
